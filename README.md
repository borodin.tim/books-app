# GraphQL project - Blogging App - built using Prisma

## A simple blogging app with registered users, posts and comments

NodeJS GrahphQL - http://localhost:4000/  
PrismaAPI GraphQL - http://localhost:4466/

---

### What and how

Udemy course lecture 46:  
Create a posgreSQL database on heroku (via plugins)  
Download pgAdmin (v3 for Ubuntu or v4 for Windows).  
Provide the connection details to heroku DB.  
Find your connected heroku DB in a list of all DBs.

install docker and docker-compose

install prisma@1.12.0
run `prisma init prisma` - provide all the necessary config data of ur database(mine was a postgres DB deployed on heroku)

run `sudo docker-compose up -d`

run `prisma deploy -e ../config/prod.env` (has to run every time datamodel.graphql is changed)
also, run `npm run get-schema`, to get graphql schema

After adding a secret key:
To work with prisma on port 4466, an Authorization token is needed.
{
"Authorization":"Bearer THE_TOKEN_HERE"
}
to generate the token, run: `prisma token -e ../config/dev.env` (or 'prod' or 'test')

to wipe the DB run `prisma delete`

---

### Deployment:

1. DB - PostgreSQL on heroku (in PGAdmin it's called "Heroku production DB")
2. Server - Prisma 1 cloud (hosts Prisma app) - runs the Prisma docker container
3. Heroku - NodeJS App - the GgraphQL API

Or use any other hosting web service that supports containers - AWS, Heroku and a service that hosts PostgreSQL DB

Deploying to prisma 1 cloud:  
`prisma deploy -e ../config/prod.env` (and `prisma token -e ../config/dev.env`)

to re-deploy a test env:

1. cd into prisma folder
2. `prisma delete -e ../config/test.env`
3. `prisma deploy -e ../config/test.env`
4. `cd ..`
5. `npm run get-schema`

---

### Testing with Jest

To run tests from a single file - run: `npm test -t user` or `npm test --user.test.js`
To run user specific tests only - run: `npm test -t Should fetch user profile`

#### Users

- [x] Should create a new user with
- [x] Should expose public author profiles
- [x] Should not login with bad credentials
- [x] Should not register(createUser) with short password
- [x] Should fetch user profile
- [x] Should not signup a user with email that is already in use
- [x] Should login and provide authentication token
- [ ] Should reject me query without authentication
- [ ] Should hide emails when fetching list of users

#### Posts

- [x] Should get published posts only
- [x] Should return my posts
- [x] Should be able to update own post
- [x] Should create post
- [x] Should delete post
- [x] Should subscribe to changes for published posts - UPDATED
- [x] Should subscribe to changes for published posts - DELETED
- [ ] Should not be able to update another users post
- [ ] Should not be able to delete another users post
- [ ] Should require authentication to create a post (could add for update and delete too)
- [ ] Should fetch published post by id
- [ ] Should fetch own post by id
- [ ] Should not fetch draft post from other user

#### Comments

- [x] Should delete own comments
- [x] Should not delete other users comments
- [x] Should subscribe to comments for a post
- [ ] Should fetch post comments
- [ ] Should create a new comment
- [ ] Should not create comment on draft post
- [ ] Should update comment
- [ ] Should not update another users comment
- [ ] Should not delete another users comment
- [ ] Should require authentication to create a comment (could add for update and delete too)
