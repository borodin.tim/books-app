import 'cross-fetch/polyfill';
import seedDatabase, { userTwo, commentOne, commentTwo, postOne } from './utils/seedDatabase';
import getClient from './utils/getClient';
import prisma from '../src/prisma';
import { deleteComment, subscribeToComments } from './utils/operations'

const client = getClient();

beforeEach(seedDatabase);

test('Should delete own comment', async () => {
    jest.setTimeout(20000);
    const client = getClient(userTwo.jwt);
    const variables = {
        id: commentOne.comment.id
    };

    const deletedComment = await client.mutate({
        mutation: deleteComment,
        variables
    });
    const commentExists = await prisma.exists.Comment({ id: commentOne.comment.id })

    expect(deletedComment.data.deleteComment.id).toBe(commentOne.comment.id);
    expect(commentExists).toBe(false);
})

test('Should not delete other users comments', async () => {
    // jest.setTimeout(20000);
    const client = getClient(userTwo.jwt);
    const variables = {
        id: commentTwo.comment.id
    };

    await expect(client.mutate({
        mutation: deleteComment,
        variables
    })).rejects.toThrow();
    const commentExists = await prisma.exists.Comment({ id: commentTwo.comment.id });

    expect(commentExists).toBe(true);
})

test('Should subscribe to comments for a post', async (done) => {
    const variables = {
        postId: postOne.post.id
    };
    client.subscribe({
        query: subscribeToComments,
        variables
    }).subscribe({
        next(response) {
            expect(response.data.comment.mutation).toBe('DELETED');
            done();
        }
    });

    await prisma.mutation.deleteComment({
        where: {
            id: commentOne.comment.id
        }
    });
})
