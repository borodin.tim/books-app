import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import prisma from '../../src/prisma';

const userOne = {
    input: {
        name: 'Bob',
        email: 'bob@mail.com',
        password: bcrypt.hashSync('asd12345', bcrypt.genSaltSync(10))
    },
    user: undefined,
    jwt: undefined
}

const userTwo = {
    input: {
        name: 'John',
        email: 'john@mail.com',
        password: bcrypt.hashSync('qwerty78', bcrypt.genSaltSync(10))
    },
    user: undefined,
    jwt: undefined
}

const postOne = {
    input: {
        title: 'Post one',
        body: 'Body of post one',
        published: true
    },
    post: undefined
}

const postTwo = {
    input: {
        title: 'Post two',
        body: 'Body of post two',
        published: false
    },
    post: undefined
}

const commentOne = {
    input: {
        text: "Body of comment 1",
    },
    comment: undefined
}

const commentTwo = {
    input: {
        text: "Body of comment 2",
    },
    comment: undefined
}

const seedDatabase = async () => {
    jest.setTimeout(50000);

    await prisma.mutation.deleteManyComments();
    await prisma.mutation.deleteManyPosts();
    await prisma.mutation.deleteManyUsers();

    await createUserInDB(userOne);
    await createUserInDB(userTwo);

    await createPostInDB(postOne, userOne.user.id);
    await createPostInDB(postTwo, userOne.user.id);

    await createCommentInDB(commentOne, userTwo.user.id, postOne.post.id);
    await createCommentInDB(commentTwo, userOne.user.id, postOne.post.id);
}

async function createUserInDB(user) {
    user.user = await prisma.mutation.createUser({
        data: user.input
    });
    user.jwt = jwt.sign({ userId: user.user.id }, process.env.SECRET_KEY_JWT);
}

async function createPostInDB(post, author) {
    post.post = await prisma.mutation.createPost({
        data: {
            ...post.input,
            author: {
                connect: {
                    id: author
                }
            }
        }
    });
}

async function createCommentInDB(comment, authorId, postId) {
    comment.comment = await prisma.mutation.createComment({
        data: {
            ...comment.input,
            author: {
                connect: {
                    id: authorId
                }
            },
            post: {
                connect: {
                    id: postId,
                }
            }
        }
    });
}

export { seedDatabase as default, userOne, userTwo, postOne, postTwo, commentOne, commentTwo };