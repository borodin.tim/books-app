import jwt from "jsonwebtoken";

const getUserId = (request, requireAuth = true) => {
    const header = request.request ?
        request.request.headers.authorization // for queries and mutations
        :
        request.connection.context.Authorization; // for subscriptions

    if (header) {
        const token = header.replace('Bearer ', '');
        const decoded = jwt.verify(token, process.env.SECRET_KEY_JWT);
        return decoded.userId;
    }
    if (requireAuth) {
        throw new Error('Authentication required');
    }
    return null;
}

export default getUserId;